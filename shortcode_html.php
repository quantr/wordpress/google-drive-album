<link href="<?= plugin_dir_url(__FILE__) ?>/library/lightbox2-2.11.3/dist/css/lightbox.min.css" rel="stylesheet" />
<script src="<?= plugin_dir_url(__FILE__) ?>/library/lightbox2-2.11.3/dist/js/lightbox.min.js"></script>

<div class="container">
	<div class="row">
		<?php
		require __DIR__ . '/vendor/autoload.php';

		// echo 'g1='.date("Y/m/d H:i:s").'<br>';
		$client = new Google_Client();
		$client->setScopes(Google_Service_Drive::DRIVE_READONLY);
		$client->setAuthConfig('/home/quantr.foundation/www/credentials.json');
		$service = new Google_Service_Drive($client);

		// Print the names and IDs for up to 10 files.
		$optParams = array(
			// 'pageSize' => 10,
			//'fields' => 'nextPageToken, files(id, name, webContentLink, mimeType, exportLinks)'
			'fields' => '*',
			'orderBy' => 'createdTime desc'
		);
		$results = $service->files->listFiles($optParams);
		// var_dump($results);

		if (count($results->getFiles()) == 0) {
			echo "No files found.<br>";
		} else {
			foreach ($results->getFiles() as $file) {
				if (strpos($file->getMimeType(), "image") !== false) {
					$fileId = $file->getId();
					// echo "<div class='col-md-2 col-sm-6' style='padding: 5px;'>".$file->getWebViewLink()."</div>";
					echo "
					<div class='col-6 col-md-2' style='padding: 5px;'>
						<div style='position: relative;'>
							<a href='" . $file->getWebContentLink() . "' data-lightbox='image-1' data-title='" . $file->description . "'>
								<img src='" . $file->getWebContentLink() . "' class='img-fluid' style='border-radius:5px;' />
							</a>
							<div style='font-size:10px; position: absolute; width: 100%; bottom: 0px; background-color: #00000088; color: white; padding: 2px;'>".$file->description."</div>
						</div>
					</div>";
				}
			}
		}
		// echo 'g2='.date("Y/m/d H:i:s").'<br>';
		?>
	</div>
</div>