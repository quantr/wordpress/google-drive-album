<?php
/*
Plugin Name: Google Drive Album
Plugin URI: https://gitlab.com/quantr/quantr-foundation-wordpress-widgets
Description: Documentation engine for wordpress
Version: 1.2.0
Author: Peter
Support URI: https://gitlab.com/quantr/quantr-foundation-wordpress-widgets
Author URI: https://gitlab.com/quantr/quantr-foundation-wordpress-widgets
License: Quantr Commercial License

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

// include(get_home_path() . '/wp-includes/pluggable.php');
// include(plugin_dir_path(__FILE__) . 'public/shortcode.php');

include(plugin_dir_path(__FILE__) . 'shortcode.php');