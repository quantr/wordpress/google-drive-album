<?php
function shortcode_init()
{
    function google_drive_album($atts = [], $content = null)
    {
		ob_start();
    	include_once('shortcode_html.php');
		$content = ob_get_clean();
		return $content;
    }
    add_shortcode('google-drive-album', 'google_drive_album');


    function google_photo_album($atts = [], $content = null)
    {
		ob_start();
    	include_once('shortcode_photo_html.php');
		$content = ob_get_clean();
		return $content;
    }
    add_shortcode('google-photo-album', 'google_photo_album');
}
add_action('init', 'shortcode_init');
